from sqlalchemy import MetaData, Table, Column, Integer, String, ForeignKey, Numeric
metadata = MetaData()

menu = Table(
    'menu',
    metadata,
    Column('id', Integer, primary_key=True),
    Column('title', String(20), nullable=False),
    Column('description', String(100))
)

submenu = Table(
    'submenu',
    metadata,
    Column('id', Integer, primary_key=True),
    Column('title', String(20), nullable=False),
    Column('description', String(100)),
    Column('menu_id', Integer, ForeignKey('menu.id'))
)

dish = Table(
    'dish',
    metadata,
    Column('id', Integer, primary_key=True),
    Column('title', String(20), nullable=False),
    Column('description', String(100)),
    Column('price', Numeric(10, 2), nullable=False),
    Column('submenu_id', Integer, ForeignKey('submenu.id'))

)