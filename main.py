from fastapi import FastAPI
from models.models import metadata


app = FastAPI(
    title='Menu App'
)


@app.post('/name')
def create_menu(title: str, description: str):
    menu.







users = [
    {'id': 1, 'role': 'admin', 'name': 'Bob'},
    {'id': 2, 'role': 'investor', 'name': 'John'},
    {'id': 3, 'role': 'trader', 'name': 'Matt'}
]

@app.get('/users/{user_id}')
def get_user(user_id: int):
    return [user for user in users if user.get('id') == user_id]

fake_trades = [
    {'id': 1, 'user_id': 1, 'currency': 'BTC', 'side': 'buy', 'price': 123, 'amount': 2.12},
    {'id': 2, 'user_id': 1, 'currency': 'BTC', 'side': 'sell', 'price': 125, 'amount': 2.12}
]

@app.get('/trades')
def get_trades(limit: int, offset: int):
    return fake_trades[offset:][:limit]

fake_users2 = [
    {'id': 1, 'role': 'admin', 'name': 'Bob'},
    {'id': 2, 'role': 'investor', 'name': 'John'},
    {'id': 3, 'role': 'trader', 'name': 'Matt'}
]


@app.post('/users/{user_id}')
def change_user_name(user_id: int, new_name: str):
    current_user = list(filter(lambda user: user.get('id') == user_id, fake_users2))[0]
    current_user['name'] = new_name
    return {'ststus': 200, 'data': current_user}
